package serviceimpl


import (
	"context"

	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"

	walletsvc "gitlab.com/Krauze67/coins-wallet/cmd/wallet"
	"gitlab.com/Krauze67/coins-wallet/cmd/wallet/model"
)

//
// service implements the Wallet Service.
//
type service struct {
	repository walletsvc.Repository
	logger     log.Logger
}

//
// NewService creates and returns a new Wallet service instance.
//
func NewService(rep walletsvc.Repository, logger log.Logger) walletsvc.Service {
	return &service{
		repository: rep,
		logger:     logger,
	}
}

//
// GetAccounts returns a list of all accounts in the system.
//
func (s *service) GetAccounts(ctx context.Context, currency string) ([]model.Account, error) {
	logger := log.With(s.logger, "method", "GetAccounts")

	accounts, err := s.repository.GetAccounts(ctx, currency)
	if err != nil {
		level.Error(logger).Log("err", err)
	}
	return accounts, err
}

//
// CreatePayment creates a new payment and returns a result.
//
func (s *service) CreatePayment(
	ctx context.Context,
	accFrom string,
	accTo string,
	amount float64,
	reason string,
) (model.Payment, error) {
	logger := log.With(s.logger, "method", "CreatePayment")

	payment, err := s.repository.CreatePayment(ctx, accFrom, accTo, amount, reason)
	if err != nil {
		level.Error(logger).Log("err", err)
	}
	return payment, err
}

//
// GetPayments returns a list of all payments in the system.
//
func (s *service) GetPayments(ctx context.Context) ([]model.Payment, error) {
	logger := log.With(s.logger, "method", "GetPayments")

	payments, err := s.repository.GetPayments(ctx)
	if err != nil {
		level.Error(logger).Log("err", err)
	}
	return payments, nil
}
