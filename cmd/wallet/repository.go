package wallet

import (
	"context"

	"gitlab.com/Krauze67/coins-wallet/cmd/wallet/model"
)

//
// Repository describes the persistence on model.
//
type Repository interface {
	GetAccounts(ctx context.Context, currency string) ([]model.Account, error)
	GetPayments(ctx context.Context) ([]model.Payment, error)
	CreatePayment(ctx context.Context, accFrom string, accTo string, amount float64, reason string) (model.Payment, error)
}
