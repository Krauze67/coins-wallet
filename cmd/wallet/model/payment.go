package model

import "time"

//
// Payment describes all info to describe Payment item.
//
type Payment struct {
	UID           string    `json:"uid"`
	AccountCredit string    `json:"from,omitempty"`
	AccountDebit  string    `json:"to,omitempty"`
	Amount        float64   `json:"amount,omitempty"`
	Currency      string    `json:"currency,omitempty"`
	Reason        string    `json:"reason,omitempty"`
	CreatedAt     time.Time `json:"created_at"`
	Status        string    `json:"status"`
}
