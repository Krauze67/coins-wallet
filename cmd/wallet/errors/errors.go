package errors

import (
	"errors"
)

//
// Wallet service errors.
//
var (
	//
	// Common ones.
	//
	ErrInvalidRequestBody = errors.New("invalid request body")
	ErrInternal           = errors.New("internal service error")

	//
	// DB errors.
	//
	ErrQueryRepository  = errors.New("unable to query repository")
	ErrResourceNotFound = errors.New("resource(s) has not been found")

	//
	// Create Payment errors.
	//
	ErrCurrencyDoesNotFit   = errors.New("sender and recipient accounts must be of the same currency")
	ErrInvalidCreditAccount = errors.New("sender account does not exist")
	ErrInvalidDebitAccount  = errors.New("recipient account does not exist")
	ErrUnsufficientFunds    = errors.New("unsufficient funds")
)
