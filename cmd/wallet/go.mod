module gitlab.com/Krauze67/coins-wallet/cmd/wallet

go 1.12

require (
	github.com/go-kit/kit v0.9.0
	github.com/go-logfmt/logfmt v0.4.0 // indirect
	github.com/go-stack/stack v1.8.0 // indirect
	gitlab.com/Krauze67/coins-wallet/cmd/wallet/errors v0.0.0-20190807192432-128ec40d7f83
	gitlab.com/Krauze67/coins-wallet/cmd/wallet/model v0.0.0-00010101000000-000000000000
)

replace (
	gitlab.com/Krauze67/coins-wallet/cmd/wallet/model => ./model
	gitlab.com/Krauze67/coins-wallet/cmd/wallet/transport => ./transport
)
