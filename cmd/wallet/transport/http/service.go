package http

import (
	"context"
	"encoding/json"
	"errors"
	"net/http"

	"github.com/go-kit/kit/log"
	kithttp "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"

	walleterrors "gitlab.com/Krauze67/coins-wallet/cmd/wallet/errors"
	"gitlab.com/Krauze67/coins-wallet/cmd/wallet/transport"
)

const (
	accountsFilterName = "filter"

	accountsFilterValCurrency = "currency"
)

var (
	//
	// ErrBadRouting to return in case no according endpoint found.
	//
	ErrBadRouting = errors.New("bad routing")
)

//
// NewService wires Go kit endpoints to the HTTP transport.
//
func NewService(
	svcEndpoints transport.Endpoints,
	logger log.Logger,
) http.Handler {
	// set-up router and initialize http endpoints

	r := mux.NewRouter()
	r.Use(commonMiddleware)

	options := []kithttp.ServerOption{
		kithttp.ServerErrorLogger(logger),
		kithttp.ServerErrorEncoder(encodeError),
	}

	// HTTP Get - /accounts
	r.Methods("GET").Path("/accounts").Handler(kithttp.NewServer(
		svcEndpoints.GetAccounts,
		decodeGetAccountsRequest,
		encodeResponse,
		options...,
	))

	// HTTP Get - /accounts/{filter}/{filterval}
	r.Methods("GET").Path("/accounts/{" + accountsFilterName + "}/{" + accountsFilterValCurrency + "}").Handler(kithttp.NewServer(
		svcEndpoints.GetAccounts,
		decodeGetAccountsRequest,
		encodeResponse,
		options...,
	))

	// HTTP Get - /payments
	r.Methods("GET").Path("/payments").Handler(kithttp.NewServer(
		svcEndpoints.GetPayments,
		decodeEmptyRequest,
		encodeResponse,
		options...,
	))

	// HTTP Post - /payment
	r.Methods("POST").Path("/payment").Handler(kithttp.NewServer(
		svcEndpoints.CreatePayment,
		decodeCreatePaymentRequest,
		encodeResponse,
		options...,
	))
	return r
}

func commonMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Content-Type", "application/json")
		next.ServeHTTP(w, r)
	})
}

func decodeGetAccountsRequest(_ context.Context, r *http.Request) (request interface{}, err error) {

	currency := ""

	vars := mux.Vars(r)
	filter := vars[accountsFilterName]
	if filter == accountsFilterValCurrency {
		currency, _ = vars[accountsFilterValCurrency]
	}

	return transport.GetAccountsRequest{Currency: currency}, nil
}

func decodeCreatePaymentRequest(_ context.Context, r *http.Request) (request interface{}, err error) {
	var req transport.CreatePaymentRequest
	if e := json.NewDecoder(r.Body).Decode(&req); e != nil {
		return nil, e
	}
	return req, nil
}

func decodeEmptyRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req transport.EmptyRequest
	return req, nil
}

type errorer interface {
	error() error
}

func encodeResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	if e, ok := response.(errorer); ok && e.error() != nil {
		// Not a Go kit transport error, but a business-logic error.
		// Provide those as HTTP errors.
		encodeError(ctx, e.error(), w)
		return nil
	}
	return kithttp.EncodeJSONResponse(ctx, w, response)
}

func encodeError(_ context.Context, err error, w http.ResponseWriter) {
	if err == nil {
		panic("encodeError with nil error")
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(codeFrom(err))
	json.NewEncoder(w).Encode(map[string]interface{}{
		"error": err.Error(),
	})
}

func codeFrom(err error) int {
	switch err {
	case walleterrors.ErrCurrencyDoesNotFit:
		return http.StatusBadRequest
	default:
		return http.StatusInternalServerError
	}
}
