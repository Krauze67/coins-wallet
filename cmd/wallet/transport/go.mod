module gitlab.com/Krauze67/coins-wallet/cmd/wallet/transport

go 1.12

require (
	github.com/go-kit/kit v0.9.0
	gitlab.com/Krauze67/coins-wallet/cmd/wallet/model v0.0.0-00010101000000-000000000000
)

replace (
	gitlab.com/Krauze67/coins-wallet/cmd/wallet => ./..
	gitlab.com/Krauze67/coins-wallet/cmd/wallet/model => ./../model
	gitlab.com/Krauze67/coins-wallet/cmd/wallet/errors => ./../errors
)
