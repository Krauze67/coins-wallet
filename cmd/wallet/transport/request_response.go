package transport

import (
	"gitlab.com/Krauze67/coins-wallet/cmd/wallet/model"
)

// EmptyRequest to handle requests with no parameters.
//
type EmptyRequest struct{}

//
// GetAccountsRequest holds information to select accounts filtered by currency.
//
type GetAccountsRequest struct {
	Currency string `json:"currency"` // optional
}

//
// GetAccountsResponse holds information about accounts selected to return.
//
type GetAccountsResponse struct {
	Accounts []model.Account `json:"accounts,omitempty"`
	Err      string          `json:"error,omitempty"`
}

//
// CreatePaymentRequest holds all parameters required to create a new payment.
//
type CreatePaymentRequest struct {
	AccountCredit string  `json:"from,omitempty"`
	AccountDebit  string  `json:"to,omitempty"`
	Amount        float64 `json:"amount,omitempty"`
	Reason        string  `json:"reason,omitempty"`
}

//
// PaymentsResponse to return payments list.
//
type PaymentsResponse struct {
	Payments []model.Payment `json:"payments,omitempty"`
	Err      string          `json:"error,omitempty"`
}
