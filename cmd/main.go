package main

import (
	"database/sql"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	kitlog "github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
	_ "github.com/lib/pq"

	"gitlab.com/Krauze67/coins-wallet/cmd/wallet"
	"gitlab.com/Krauze67/coins-wallet/cmd/wallet/config"
	repo "gitlab.com/Krauze67/coins-wallet/cmd/wallet/repository"
	walletsvc "gitlab.com/Krauze67/coins-wallet/cmd/wallet/serviceimpl"
	"gitlab.com/Krauze67/coins-wallet/cmd/wallet/transport"
	httptransport "gitlab.com/Krauze67/coins-wallet/cmd/wallet/transport/http"
)

func main() {

	// Config
	if err := config.Parameters.Load(); err != nil {
		panic(err)
	}

	// Logger
	logger := initLogger()
	level.Info(logger).Log("msg", "service started")
	defer level.Info(logger).Log("msg", "service ended")

	// Database
	var db *sql.DB
	{
		level.Info(logger).Log("msg", "creating a DB connection")
		var err error
		// Connect to the database
		db, err = sql.Open(
			config.Parameters.GetDBDriver(),
			config.Parameters.DatabaseDSN,
		)
		if err != nil {
			level.Error(logger).Log("exit", err)
			os.Exit(-1)
		}
		level.Info(logger).Log("msg", "DB connection has been created")
	}

	// Create Wallet Service
	var svc wallet.Service
	{
		level.Info(logger).Log("msg", "creating a service")
		repository, err := repo.New(db, logger)
		if err != nil {
			level.Error(logger).Log("exit", err)
			os.Exit(-1)
		}

		svc = walletsvc.NewService(repository, logger)
		level.Info(logger).Log("msg", "service instance has been created")
	}

	// create HTTP handler
	var h http.Handler
	{
		level.Info(logger).Log("msg", "HTTP transport init")
		endpoints := transport.MakeEndpoints(svc)
		h = httptransport.NewService(endpoints, logger)
		level.Info(logger).Log("msg", "HTTP transport init done")
	}

	errs := make(chan error)
	go func() {
		c := make(chan os.Signal)
		signal.Notify(c, syscall.SIGINT, syscall.SIGTERM)
		errs <- fmt.Errorf("%s", <-c)
	}()

	go func() {
		level.Info(logger).Log("transport", "HTTP", "addr", config.Parameters.ListenAddress)
		server := &http.Server{
			Addr:        config.Parameters.ListenAddress,
			Handler:     h,
			ReadTimeout: time.Duration(config.Parameters.HTTPTimeoutSec) * time.Second,
		}
		errs <- server.ListenAndServe()
	}()

	level.Error(logger).Log("exit", <-errs)
}

func initLogger() kitlog.Logger {

	logger := kitlog.NewLogfmtLogger(os.Stderr)
	logger = kitlog.NewSyncLogger(logger)
	logger = level.NewFilter(logger, getLogLevelOption(config.Parameters.LogLevel))
	logger = kitlog.With(logger,
		"svc", config.ServiceName,
		"ts", kitlog.DefaultTimestampUTC,
		"caller", kitlog.DefaultCaller,
	)
	return logger
}

func getLogLevelOption(lvl string) level.Option {
	switch lvl {
	case "DEBUG":
		{
			return level.AllowDebug()
		}
	case "INFO":
		{
			return level.AllowInfo()
		}
	case "WARNING":
		{
			return level.AllowWarn()
		}
	case "ERROR":
		{
			return level.AllowError()
		}
	}
	return level.AllowWarn()
}
