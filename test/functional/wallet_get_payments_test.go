// +build functional

package wallet

import (
	"io/ioutil"
	"net/http"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestGetPayments(t *testing.T) {

	httpRequest, err := http.NewRequest(
		http.MethodGet,
		URLBase+"/payments",
		nil,
	)
	assert.Nil(t, err)

	HTTPClient := &http.Client{
		Timeout: time.Second * 5,
	}

	httpResponse, err := HTTPClient.Do(httpRequest)
	assert.Nil(t, err)
	assert.Equal(t, http.StatusOK, httpResponse.StatusCode)

	defer func() {
		assert.Nil(t, httpResponse.Body.Close())
	}()

	_, err = ioutil.ReadAll(httpResponse.Body)
	assert.Nil(t, err)
}
