package wallet

//
// URLs.
//
var (
	URLBase = "http://coins-wallet:8080"
)

//
// Requests.
//
var (
	PaymentRequest01FieldFrom = "Alfred01-Us"
	PaymentRequest01FieldTo   = "Sam1999"
	PaymentRequest01          = []byte(`{
		"from": "` + PaymentRequest01FieldFrom + `",
		"to": "` + PaymentRequest01FieldTo + `",
		"amount": 3.05,
		"reason": "charity"
		}`)
)

//
// Responses.
//
var (
	PaymentResponse01FieldFrom   = PaymentRequest01FieldFrom
	PaymentResponse01FieldTo     = PaymentRequest01FieldTo
	PaymentResponse01FieldStatus = "done"
)
