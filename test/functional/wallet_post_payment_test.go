// +build functional

package wallet

import (
	"bytes"
	"io/ioutil"
	"net/http"
	"testing"
	"time"

	"github.com/buger/jsonparser"
	"github.com/stretchr/testify/assert"
)

func TestPostPaymentBadBody(t *testing.T) {

	httpRequest, err := http.NewRequest(
		http.MethodPost,
		URLBase+"/payment",
		bytes.NewReader([]byte("some invalid data")),
	)
	assert.Nil(t, err)

	HTTPClient := &http.Client{
		Timeout: time.Second * 5,
	}

	httpResponse, err := HTTPClient.Do(httpRequest)
	assert.Nil(t, err)
	assert.Equal(t, http.StatusInternalServerError, httpResponse.StatusCode)

	defer func() {
		assert.Nil(t, httpResponse.Body.Close())
	}()
}

func TestPostPaymentCorrect(t *testing.T) {

	httpRequest, err := http.NewRequest(
		http.MethodPost,
		URLBase+"/payment",
		bytes.NewReader(PaymentRequest01),
	)
	assert.Nil(t, err)

	HTTPClient := &http.Client{
		Timeout: time.Second * 5,
	}

	httpResponse, err := HTTPClient.Do(httpRequest)
	assert.Nil(t, err)
	assert.Equal(t, http.StatusOK, httpResponse.StatusCode)

	defer func() {
		assert.Nil(t, httpResponse.Body.Close())
	}()

	body, err := ioutil.ReadAll(httpResponse.Body)
	assert.Nil(t, err)

	// check fields
	val, err := jsonparser.GetString(body, "payments", "[0]", "from")
	assert.Nil(t, err)
	assert.Equal(t, val, PaymentResponse01FieldFrom)

	val, err = jsonparser.GetString(body, "payments", "[0]", "to")
	assert.Nil(t, err)
	assert.Equal(t, val, PaymentResponse01FieldTo)

	val, err = jsonparser.GetString(body, "payments", "[0]", "status")
	assert.Nil(t, err)
	assert.Equal(t, val, PaymentResponse01FieldStatus)
}
