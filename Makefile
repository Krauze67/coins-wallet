# Makefile.
.DEFAULT_GOAL := build

# Target list.
.PHONY: init build clean
.PHONY: go_get go_build go_lint go_test_unit go_test_functional
.PHONY: docker_image
.PHONY: cluster_start cluster_stop cluster_restart cluster_test cluster_run_tests

export GO111MODULE=on

# Service name. 
SERVICE_NAME=coins-wallet

# Path to service entry point.
GO_PATH_SERVICE_MAIN=./cmd

#
# General variables
#
# .netrc file path. It's content is filled by the TravisCI and is used for Github authentication for private
# repositories.
NETRC_CONTENT=$(shell cat ~/.netrc)
# Path to Docker file.
PATH_DOCKER_FILE=$(realpath ./build/Dockerfile)
# Path to docker-compose file.
PATH_DOCKER_COMPOSE_FILE=$(realpath ./build/docker-compose.yml)
# Service go module import path.
GO_SERVICE_IMPORT_PATH=$(shell go list .)
# Docker compose starting options.
DOCKER_COMPOSE_OPTIONS= -f $(PATH_DOCKER_COMPOSE_FILE)

# Packages covered with unit tests.
GO_UNIT_TESTED_PACKAGES=$(shell go list ./...)

# Go build flags.
GO_BUILD_FLAGS=-v 

# Build LD flags. Beware that MacOS build is dynamic one.
ifneq ($(shell go env GOOS),darwin)
	GO_BUILD_FLAGS+= --ldflags "-linkmode external -extldflags '-static'"
endif

#
# Build targets
#

init:
	@echo '>>> Initialize the project.'
	@if [ "Darwin" = "$(shell uname -s)" ]; then \
		sed -i '' -e "s/{% SERVICE_NAME %}/$(SERVICE_NAME)/g" $(PATH_DOCKER_FILE) $(PATH_DOCKER_COMPOSE_FILE); \
	else \
		sed -i'' "s/{% SERVICE_NAME %}/$(SERVICE_NAME)/g" $(PATH_DOCKER_FILE) $(PATH_DOCKER_COMPOSE_FILE); \
	fi;  \

build: clean go_get go_test_unit go_build

clean: 
	@echo ">>> Clean up."
	@rm -rvf $(SERVICE_NAME)

#
# Go targets.
#
go_get:
	@go help mod > /dev/null || (@echo build requires Go version 1.11 or higher && exit 1)
	@echo '>>> Getting go modules.'

go_build:
	@echo '>>> Building go binary.'
	@go build -mod vendor -race $(GO_BUILD_FLAGS) -o $(SERVICE_NAME) $(GO_PATH_SERVICE_MAIN);

go_lint:
	@docker run --rm -t  \
		-v $$PWD:/go/cmd/$(GO_SERVICE_IMPORT_PATH)  \
		-w /go/cmd/$(GO_SERVICE_IMPORT_PATH) roistat/golangci-lint

go_test_unit:
	@echo ">>> Running unit tests."
	go test -cover $(GO_UNIT_TESTED_PACKAGES)

go_test_functional:
	@echo ">>> Running functional tests."
	@go test -v -tags="functional" ./test/functional/...

#
# Docker targets.
#
# TODO(DF) remove --no-cache option.
docker_image:
	@echo ">>> Building docker image with service binary."
	docker build --no-cache \
		-t $(SERVICE_NAME) \
		--build-arg NETRC_CONTENT="$(NETRC_CONTENT)" \
		--build-arg SERVICE_NAME=$(SERVICE_NAME) \
		--build-arg GO_SERVICE_IMPORT_PATH=$(GO_SERVICE_IMPORT_PATH) \
		-f $(PATH_DOCKER_FILE) \
		.

#
# Cluster targets.
#
cluster_build:
	@docker-compose $(DOCKER_COMPOSE_OPTIONS) build --no-cache

cluster_start: cluster_build
	@echo ">>> Starting cluster."
	@docker-compose $(DOCKER_COMPOSE_OPTIONS) up -d wallet-postgres
	@echo ">>> Sleeping 10 seconds to be sure PostgreSQL fixtures are applied."
	@sleep 10
	@echo ">>> Starting wallet container."
	@docker-compose $(DOCKER_COMPOSE_OPTIONS) up -d $(SERVICE)

cluster_stop:
	@echo ">>> Stopping cluster."
	@docker-compose $(DOCKER_COMPOSE_OPTIONS) down

cluster_restart: cluster_stop cluster_start

cluster_run_tests:
	@echo ">>> Running tests over cluster."
	@docker-compose $(DOCKER_COMPOSE_OPTIONS) run \
		--rm \
		-v $$PWD:/go/src/$(GO_SERVICE_IMPORT_PATH) \
		-w /go/src/$(GO_SERVICE_IMPORT_PATH) \
		--no-deps \
		tests

cluster_test: cluster_stop cluster_start cluster_run_tests cluster_stop

