module coins-wallet

go 1.12

require (
	github.com/buger/jsonparser v0.0.0-20181115193947-bf1c66bbce23
	github.com/go-kit/kit v0.9.0
	github.com/gorilla/mux v1.7.3 // indirect
	github.com/kelseyhightower/envconfig v1.4.0 // indirect
	github.com/lib/pq v1.2.0
	github.com/stretchr/testify v1.3.0
	gitlab.com/Krauze67/coins-wallet/cmd/wallet v0.0.0-00010101000000-000000000000
	gitlab.com/Krauze67/coins-wallet/cmd/wallet/config v0.0.0-00010101000000-000000000000
	gitlab.com/Krauze67/coins-wallet/cmd/wallet/transport v0.0.0-00010101000000-000000000000
)

replace (
	gitlab.com/Krauze67/coins-wallet/cmd/wallet => ./cmd/wallet
	gitlab.com/Krauze67/coins-wallet/cmd/wallet/config => ./cmd/wallet/config
	gitlab.com/Krauze67/coins-wallet/cmd/wallet/errors => ./cmd/wallet/errors
	gitlab.com/Krauze67/coins-wallet/cmd/wallet/model => ./cmd/wallet/model
	gitlab.com/Krauze67/coins-wallet/cmd/wallet/transport => ./cmd/wallet/transport
)
