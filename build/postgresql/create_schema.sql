
GRANT ALL PRIVILEGES ON DATABASE wallet TO postgres;

CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE IF NOT EXISTS currency (
   id serial PRIMARY KEY,
   name VARCHAR (100) UNIQUE NOT NULL CHECK(length(name)>0),
   divider bigint NOT NULL CHECK(divider>0) DEFAULT 100
);

CREATE TABLE IF NOT EXISTS account (
   id serial PRIMARY KEY,
   name VARCHAR (100) UNIQUE NOT NULL CHECK(length(name)>0),
   currency_id INTEGER NOT NULL,
   balance bigint NOT NULL DEFAULT 0,
   CONSTRAINT account_currency_id_fkey FOREIGN KEY (currency_id)
      REFERENCES currency(id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION 
);

CREATE TABLE IF NOT EXISTS payment (
   id serial PRIMARY KEY,
   uid uuid UNIQUE NOT NULL DEFAULT uuid_generate_v4(),

   account_credit INTEGER NOT NULL,
   account_debit INTEGER NOT NULL,
   amount bigint NOT NULL CHECK(amount>0),
   created_at TIMESTAMP NOT NULL CHECK(created_at > NOW() - INTERVAL '2 minutes' AND created_at <= now()) DEFAULT CURRENT_TIMESTAMP,
   reason VARCHAR (500) NOT NULL,
   status INTEGER NOT NULL DEFAULT 0,
   processed_at TIMESTAMP NOT NULL CHECK(processed_at <= now()) DEFAULT to_timestamp(0),
   description VARCHAR (1000),

   CONSTRAINT payment_account_credit_fkey FOREIGN KEY (account_credit)
      REFERENCES account(id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
   CONSTRAINT payment_account_debit_fkey FOREIGN KEY (account_debit)
      REFERENCES account(id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);

