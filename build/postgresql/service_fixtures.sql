
INSERT INTO currency (id, name, divider) VALUES 
(1, 'USD', 100),
(2, 'CHF', 100),
(3, 'JPY', 100),
(4, 'BTC', 1000000000)
ON CONFLICT DO NOTHING;

INSERT INTO account (id, name, currency_id, balance) VALUES 
(1, 'Alfred01-Us', 1, 99905),
(2, 'Sam1999', 1, 1012),
(3, 'Nobel, Alfred', 1, 100000000),
(4, 'Katy21', 1, 10175),
(5, 'Picachu', 3, 7380000),
(6, 'Hacker-BG', 4, 200000000000000),
(7, 'Just a Good Person', 4, 47005)
ON CONFLICT DO NOTHING;

