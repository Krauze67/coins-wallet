package config

import (
	"errors"
	"strings"

	"github.com/kelseyhightower/envconfig"
)

//
// ServiceName to use in log, events etc.
//
const ServiceName = "dfwallet"

//
// Default config constants.
//
const (
	DefListenAddress  = ":8088"
	DefLogLevel       = "WARNING"
	DefHTTPTimeoutSec = 5
)

//
// Config contains parameters required to run the service.
//
type Config struct {
	ListenAddress  string `envconfig:"WALLET_LISTEN_ADDRESS", required:"false"`
	DatabaseDSN    string `envconfig:"WALLET_DB_DSN", required:"true"`
	LogLevel       string `envconfig:"WALLET_LOG_LEVEL", required:"false"`
	HTTPTimeoutSec int    `envconfig:"WALLET_HTTP_TIMEOUT_SEC", required:"false"`
}

var (
	//
	// Parameters contain all parameters to start the service.
	//
	Parameters = &Config{
		ListenAddress: DefListenAddress,
		LogLevel:      DefLogLevel,
	}
)

//
// Load loads parameters from environment variables to config.
//
func (c *Config) Load() error {

	err := envconfig.Process(ServiceName, c)
	if err != nil {
		return err
	}

	if err = c.Validate(); err != nil {
		return err
	}

	return nil
}

//
// Validate validates required parameters etc.
//
func (c *Config) Validate() error {

	if c.DatabaseDSN == "" {
		return errors.New("required config parameter WALLET_DB_DSN is empty")
	}

	return nil
}

//
// GetDBDriver parses DB driver name based on current DSN value.
//
func (c *Config) GetDBDriver() string {
	if strings.HasPrefix(c.DatabaseDSN, "postgres://") {
		return "postgres"
	}

	// TODO(DF): add other drivers.

	return ""
}
