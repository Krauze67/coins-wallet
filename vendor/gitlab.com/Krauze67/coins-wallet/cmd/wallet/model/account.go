package model

//
// Account describes account info.
//
type Account struct {
	Name     string  `json:"name,omitempty"`
	Currency string  `json:"currency,omitempty"`
	Balance  float64 `json:"balance,omitempty"`
}
