package model

//
// PaymentStatus used to get correct balance for new payments.
//
type PaymentStatus int

//
// According PaymentStatus constants.
//
const (
	PaymentPending  PaymentStatus = 0
	PaymentReserved PaymentStatus = 1
	PaymentCharged  PaymentStatus = 2
	PaymentDone     PaymentStatus = 3
	PaymentFailed   PaymentStatus = 99
)

func (s PaymentStatus) String() string {
	switch s {
	case PaymentPending:
		return "pending"
	case PaymentReserved:
		return "funds reserved"
	case PaymentCharged:
		return "charged"
	case PaymentDone:
		return "done"
	case PaymentFailed:
		return "failed"
	}
	return "invalid status"
}
