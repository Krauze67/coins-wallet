package model

//
// Currency contains all info to describe a currency item.
//
type Currency struct {
	Name    string `json:"name,omitempty"`
	Divider int64  `json:"divider,omitempty"`
}
