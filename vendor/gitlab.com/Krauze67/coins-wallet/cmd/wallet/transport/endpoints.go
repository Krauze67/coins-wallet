package transport

import (
	"context"

	"gitlab.com/Krauze67/coins-wallet/cmd/wallet"
	"gitlab.com/Krauze67/coins-wallet/cmd/wallet/model"
	walleterrors "gitlab.com/Krauze67/coins-wallet/cmd/wallet/errors"

	"github.com/go-kit/kit/endpoint"
)

//
// Endpoints holds all Go kit endpoints for the Order service.
//
type Endpoints struct {
	GetAccounts   endpoint.Endpoint
	GetPayments   endpoint.Endpoint
	CreatePayment endpoint.Endpoint
}

//
// MakeEndpoints initializes all Go kit endpoints for the Order service.
//
func MakeEndpoints(s wallet.Service) Endpoints {
	return Endpoints{
		GetAccounts:   makeGetAccountsEndpoint(s),
		GetPayments:   makeGetPaymentsEndpoint(s),
		CreatePayment: makeCreatePaymentEndpoint(s),
	}
}

func makeGetAccountsEndpoint(s wallet.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {

		currency := ""
		if req, ok := request.(GetAccountsRequest); ok {
			currency = req.Currency
		}
		accounts, err := s.GetAccounts(ctx, currency)
		response := GetAccountsResponse{
			Accounts: make([]model.Account, len(accounts)),
			Err:      "",
		}
		if err != nil {
			response.Err = err.Error()
		}

		copy(response.Accounts, accounts)

		return response, nil
	}
}

func makeGetPaymentsEndpoint(s wallet.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {

		payments, err := s.GetPayments(ctx)
		response := PaymentsResponse{
			Payments: make([]model.Payment, len(payments)),
			Err:      "",
		}
		if err != nil {
			response.Err = err.Error()
		}

		copy(response.Payments, payments)

		return response, nil
	}
}

func makeCreatePaymentEndpoint(s wallet.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {

		req, ok := request.(CreatePaymentRequest)
		if !ok {
			return model.Payment{}, walleterrors.ErrInvalidRequestBody
		}
		payment, err := s.CreatePayment(ctx, req.AccountCredit, req.AccountDebit, req.Amount, req.Reason)

		response := PaymentsResponse{
			Payments: make([]model.Payment, 0),
			Err:      "",
		}
		if err != nil {
			response.Err = err.Error()
		}
		if payment.UID != "" {
			response.Payments = append(response.Payments, payment)
		}
		return response, nil
	}
}
