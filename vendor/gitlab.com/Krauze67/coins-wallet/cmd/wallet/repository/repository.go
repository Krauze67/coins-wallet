package repository

import (
	"context"
	"database/sql"
	"fmt"
	"time"

	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
	"gitlab.com/Krauze67/coins-wallet/cmd/wallet"
	"gitlab.com/Krauze67/coins-wallet/cmd/wallet/model"
	walleterrors "gitlab.com/Krauze67/coins-wallet/cmd/wallet/errors"
)

//
// Database table names.
//
const (
	//
	// DB details.
	//
	tableCurrency = "currency"
	tableAccount  = "account"
	tablePayment  = "payment"

	//
	// log constants.
	//
	errorMsgKey = "errstr"

	//
	// Payment descriptions.
	//
	descrUnsufficientFunds = "unsufficient funds"
	descrFundsReturned     = "funds returned"
)

type repository struct {
	db     *sql.DB
	logger log.Logger
}

//
// New creates a repository instance.
//
func New(db *sql.DB, logger log.Logger) (wallet.Repository, error) {

	return &repository{
		db:     db,
		logger: log.With(logger, "rep", "sqlDB"),
	}, nil
}

//
// GetAccounts returns all accounts of specified currency.
//
func (repo *repository) GetAccounts(ctx context.Context, currency string) ([]model.Account, error) {

	// select from accounts and currencies.
	query := fmt.Sprintf(`
	SELECT %[1]s.name as acc_name, %[2]s.name as cur_name, %[1]s.balance, %[2]s.divider FROM %[1]s 
		INNER JOIN %[2]s ON %[1]s.currency_id=%[2]s.id`,
		tableAccount, tableCurrency)

	if currency != "" {
		query += fmt.Sprintf(` WHERE %s.name='%s'`, tableCurrency, currency)
	}

	rows, err := repo.db.Query(query)
	if err != nil {
		level.Error(repo.logger).Log(errorMsgKey, err.Error())
		return []model.Account{}, walleterrors.ErrQueryRepository
	}
	defer rows.Close()

	var (
		currencyDivider int64
		balance         int64
		account         model.Account
	)
	accounts := make([]model.Account, 0)
	for rows.Next() {

		if err = rows.Scan(&account.Name, &account.Currency, &balance, &currencyDivider); err != nil {
			level.Error(repo.logger).Log(errorMsgKey, err.Error())
			return []model.Account{}, walleterrors.ErrInternal
		}
		// correct a balance to a proper view.
		account.Balance = float64(balance) / float64(currencyDivider)

		accounts = append(accounts, account)
	}

	return accounts, nil
}

//
// GetPayments returns all payments registered.
//
func (repo *repository) GetPayments(ctx context.Context) ([]model.Payment, error) {

	// select from payments, accounts and currencies.

	query := fmt.Sprintf(`
	SELECT %[1]s.uid, %[1]s.created_at, acc1.name as acc_from, acc2.name as acc_to, %[1]s.amount, 
		%[3]s.divider as cur_divider, %[3]s.name as cur_name, %[1]s.reason, %[1]s.status
    FROM %[1]s INNER JOIN %[2]s as acc1 ON %[1]s.account_credit = acc1.id
        INNER JOIN %[2]s as acc2 ON %[1]s.account_debit = acc2.id
		INNER JOIN %[3]s ON acc2.currency_id=currency.id;`,
		tablePayment,
		tableAccount,
		tableCurrency)

	rows, err := repo.db.Query(query)
	if err != nil {
		level.Error(repo.logger).Log(errorMsgKey, err.Error())
		return []model.Payment{}, walleterrors.ErrQueryRepository
	}
	defer rows.Close()

	var (
		payment         model.Payment
		amount          int64
		currencyDivider int64
		status          model.PaymentStatus
	)
	payments := make([]model.Payment, 0)
	for rows.Next() {

		if err = rows.Scan(
			&payment.UID,
			&payment.CreatedAt,
			&payment.AccountCredit,
			&payment.AccountDebit,
			&amount,
			&currencyDivider,
			&payment.Currency,
			&payment.Reason,
			&status,
		); err != nil {
			level.Error(repo.logger).Log(errorMsgKey, err.Error())
			return []model.Payment{}, walleterrors.ErrInternal
		}
		// correct an amount to a proper view
		payment.Amount = float64(amount) / float64(currencyDivider)
		payment.Status = status.String()

		payments = append(payments, payment)
	}

	return payments, nil

}

//
// CreatePayment to create a new payment between two accounts of the same currency.
//
func (repo *repository) CreatePayment(ctx context.Context, accFrom string, accTo string, amount float64, reason string) (model.Payment, error) {
	if accFrom == accTo {
		return model.Payment{}, walleterrors.ErrInvalidDebitAccount
	}

	// check AccFrom.
	accIDCredit, accountCredit, currencyCredit, err := repo.getAccountAndCurrency(accFrom)
	if err == walleterrors.ErrResourceNotFound || accountCredit == nil {
		return model.Payment{}, walleterrors.ErrInvalidCreditAccount
	}
	if err != nil {
		return model.Payment{}, err
	}

	// check AccTo.
	accIDDebit, accountDebit, currencyDebit, err := repo.getAccountAndCurrency(accTo)
	if err == walleterrors.ErrResourceNotFound || accountDebit == nil {
		return model.Payment{}, walleterrors.ErrInvalidDebitAccount
	}
	if err != nil {
		return model.Payment{}, err
	}

	// check Currency.
	if currencyCredit.Name != currencyDebit.Name {
		return model.Payment{}, walleterrors.ErrCurrencyDoesNotFit
	}

	// NOTE(DF): do not check balance here. In a business logic client part checks a balance before
	// creating payment and even in case current payment is too big for some reason
	// a payment transaction will be recorded in DB as 'failed' at later step.

	// payment step #1: create a pending payment.
	amountDBValue := int64(amount * float64(currencyCredit.Divider))

	paymentID, paymentUID, createdAt, err := repo.createPaymentPending(
		accIDCredit,
		accIDDebit,
		amountDBValue,
		reason)
	if err != nil {
		return model.Payment{}, err
	}

	// payment step #2: reserve funds.
	fundsReserved, err := repo.reserveFunds(paymentID, accIDCredit, amountDBValue)
	if err != nil {
		return model.Payment{}, err
	}

	if !fundsReserved {
		// the only reason is a case of unsufficient funds.
		if _, err := repo.cancelPending(paymentID, descrUnsufficientFunds); err != nil {
			return model.Payment{}, err
		}
		return model.Payment{
			UID:           paymentUID,
			AccountCredit: accFrom,
			AccountDebit:  accTo,
			Amount:        amount,
			Reason:        reason,
			CreatedAt:     createdAt,
			Status: fmt.Sprintf(
				"%s: %s",
				model.PaymentFailed.String(),
				descrUnsufficientFunds),
		}, nil
	}

	// payment step #4: deliver funds.
	fundsDelivered, err := repo.deliverFunds(
		paymentID,
		accIDDebit,
		amountDBValue,
	)
	if err != nil {
		return model.Payment{}, err
	}

	if !fundsDelivered {
		// should not occur
		if _, err := repo.returnReservedFunds(
			paymentID,
			accIDCredit,
			amountDBValue,
			descrFundsReturned); err != nil {

			return model.Payment{}, err
		}
		return model.Payment{
			UID:           paymentUID,
			AccountCredit: accFrom,
			AccountDebit:  accTo,
			Amount:        amount,
			Reason:        reason,
			CreatedAt:     createdAt,
			Status: fmt.Sprintf(
				"%s: %s",
				model.PaymentFailed.String(),
				descrFundsReturned),
		}, nil
	}

	// at this point all is OK.
	return model.Payment{
		UID:           paymentUID,
		AccountCredit: accFrom,
		AccountDebit:  accTo,
		Amount:        amount,
		Reason:        reason,
		CreatedAt:     createdAt,
		Status:        model.PaymentDone.String(),
	}, nil
}

//
// getAccountAndCurrency retrieves Account and according Currency items from the repository.
//
func (repo *repository) getAccountAndCurrency(accountName string) (int64, *model.Account, *model.Currency, error) {

	query := fmt.Sprintf(`
	SELECT %[1]s.id, %[1]s.balance, %[2]s.name as cur_name, %[2]s.divider FROM %[1]s 
	INNER JOIN %[2]s ON %[1]s.currency_id=%[2]s.id WHERE %[1]s.name=$1`,
		tableAccount, tableCurrency)

	var (
		accountID int64
		account   model.Account
		currency  model.Currency
	)
	err := repo.db.QueryRow(query, accountName).Scan(
		&accountID,
		&account.Balance,
		&currency.Name,
		&currency.Divider)

	switch {
	case err == sql.ErrNoRows:
		return 0, nil, nil, walleterrors.ErrResourceNotFound
	case err != nil:
		level.Error(repo.logger).Log(errorMsgKey, err.Error())
		return 0, nil, nil, walleterrors.ErrInternal
	}

	account.Name = accountName
	account.Currency = currency.Name
	return accountID, &account, &currency, nil
}

//
// createPaymentPending creates a payment in 'pending' state.
//
func (repo *repository) createPaymentPending(
	accIDCredit,
	accIDDebit,
	amountDBValue int64,
	reason string,
) (paymentID int64, paymentUID string, createdAt time.Time, err error) {

	query := fmt.Sprintf(`
	INSERT INTO %s (account_credit, account_debit, amount, reason) VALUES ($1,$2,$3,$4)
	RETURNING id, uid, created_at`,
		tablePayment)

	// create payment in "pending" state:
	// NOTE(DF): for most financial processes there is no other way to create a transaction
	// and then it will be processed/rejected/failed after some period of time(in most cases by another service).
	if err = repo.db.QueryRow(query, accIDCredit, accIDDebit, amountDBValue, reason).
		Scan(&paymentID, &paymentUID, &createdAt); err != nil {

		level.Error(repo.logger).Log(errorMsgKey, err.Error())
		return 0, "", time.Time{}, walleterrors.ErrInternal
	}

	return paymentID, paymentUID, createdAt, nil
}

//
// reserveFunds moves a payment from 'pending' to 'reserved' state.
// It updates status to 'reserved' if only the payment amount is bigger
// than a current account balance.
//
func (repo *repository) reserveFunds(
	paymentID int64,
	accountCreditID int64,
	amountDB int64,
) (bool, error) {

	query := fmt.Sprintf(`
	with balance_update as (
		UPDATE %[1]s
		 SET balance = balance - %[3]d
		WHERE id = %[4]d AND balance >= %[3]d
		RETURNING id as updated_acc_id
	  )
	  UPDATE %[2]s 
		SET status=%[5]d
	  WHERE id = %[6]d
	  	AND	account_credit IN (select updated_acc_id from balance_update);`,
		tableAccount,          //1
		tablePayment,          //2
		amountDB,              //3
		accountCreditID,       //4
		model.PaymentReserved, //5
		paymentID,             //6
	)

	result, err := repo.db.Exec(query)
	if err != nil {
		level.Error(repo.logger).Log(errorMsgKey, err.Error())
		return false, walleterrors.ErrQueryRepository
	}
	rows, err := result.RowsAffected()
	if err != nil {
		level.Error(repo.logger).Log(errorMsgKey, err.Error())
		return false, walleterrors.ErrQueryRepository
	}
	return rows > 0, nil
}

//
// deliverFunds moves a payment from 'reserved' to 'finished' state.
// It updates status to 'finished' and increase a target account balance.
//
func (repo *repository) deliverFunds(
	paymentID int64,
	accountDebitID int64,
	amountDB int64,
) (bool, error) {

	query := fmt.Sprintf(`
	with balance_update as (
		UPDATE %[1]s
		 SET balance = balance + %[3]d
		WHERE id = %[4]d
		RETURNING id as updated_acc_id
	  )
	  UPDATE %[2]s 
		SET status=%[5]d, processed_at=CURRENT_TIMESTAMP
	  WHERE id = %[6]d
	  	AND	account_debit IN (select updated_acc_id from balance_update);`,
		tableAccount,      //1
		tablePayment,      //2
		amountDB,          //3
		accountDebitID,    //4
		model.PaymentDone, //5
		paymentID,         //6
	)

	result, err := repo.db.Exec(query)
	if err != nil {
		level.Error(repo.logger).Log(errorMsgKey, err.Error())
		return false, walleterrors.ErrQueryRepository
	}
	rows, err := result.RowsAffected()
	if err != nil {
		level.Error(repo.logger).Log(errorMsgKey, err.Error())
		return false, walleterrors.ErrQueryRepository
	}
	return rows > 0, nil
}

//
// cancelPending cancels a payment in a 'pending' state and adds a description.
//
func (repo *repository) cancelPending(paymentID int64, description string) (bool, error) {

	query := fmt.Sprintf(`
	  UPDATE %s 
		SET status=$1, description=$2
	  WHERE id = $3
	  	AND	status=$4;`,
		tablePayment,
	)

	result, err := repo.db.Exec(
		query,
		model.PaymentFailed,
		description,
		paymentID,
		model.PaymentPending,
	)
	if err != nil {
		level.Error(repo.logger).Log(errorMsgKey, err.Error())
		return false, walleterrors.ErrQueryRepository
	}
	rows, err := result.RowsAffected()
	if err != nil {
		level.Error(repo.logger).Log(errorMsgKey, err.Error())
		return false, walleterrors.ErrQueryRepository
	}
	return rows > 0, nil
}

//
// returnReservedFunds cancels a payment in a 'reserved' state and adds a description.
//
func (repo *repository) returnReservedFunds(
	paymentID int64,
	accountCreditID int64,
	amountDB int64,
	description string,
) (bool, error) {

	query := fmt.Sprintf(`
	with balance_update as (
		UPDATE %[1]s
		 SET balance = balance + %[3]d
		WHERE id = %[4]d 
		RETURNING id as updated_acc_id
	  )
	  UPDATE %[2]s 
		SET status=%[5]d, processed_at=CURRENT_TIMESTAMP, description=$1
	  WHERE id = %[6]d
	  	AND	account_credit IN (select updated_acc_id from balance_update);`,
		tableAccount,        //1
		tablePayment,        //2
		amountDB,            //3
		accountCreditID,     //4
		model.PaymentFailed, //5
		paymentID,           //6
	)

	result, err := repo.db.Exec(query, description)
	if err != nil {
		level.Error(repo.logger).Log(errorMsgKey, err.Error())
		return false, walleterrors.ErrQueryRepository
	}
	rows, err := result.RowsAffected()
	if err != nil {
		level.Error(repo.logger).Log(errorMsgKey, err.Error())
		return false, walleterrors.ErrQueryRepository
	}
	return rows > 0, nil
}
