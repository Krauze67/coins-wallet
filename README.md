# Coins Wallet service task v0.1

# Topics
* [Overview](#overview)
* [Parameters](#Parameters)
* [Build & Install](#build-&-install)
* [Install](#install)
* [Endpoints](#endpoints) 
  * [GET /accounts](#get-accounts)
  * [GET /payments](#get-payments)
  * [POST /payment](#post-payment)

# Overview
A `Coins Wallet service` is a test code to implement some kind of financial service RESTful backend using `go-kit`. 
At first stage the service do not have any authentication and accounts/users management. All accounts to test payments creating are added automatically while DB layer (PostgreSQL) docker container is created.
Accounts data is in `build/postgresql/service_fixtures.sql` file.

# Parameters
`Coins Wallet service` uses next listed environment variables to load its configuration:

| Env. variable         |         Content              | Default value
|-----------------------|--------------------------------------------|
| WALLET_LOG_LEVEL      | Log level. Allowed values are: `DEBUG`, `INFO`, `WARNING`, `ERROR`          | `WARNING`
| WALLET_LISTEN_ADDRESS | An `[address]:port` to listen.          | `:8080`
| WALLET_DB_DSN         | A full database DSN to connect to PostgreSQL DB layer. e.g. `postgres://postgres:postgres@wallet-postgres:5432/wallet?sslmode=disable` |


# Build & Install
The project uses `go mod` feature to vendor packages, so use *Go* of version *1.11* or higher and set **GO111MODULE** environment variable to `on`.

You could build the project as usual, by run `go get && go build` inside `./cmd` folder.
There is recommended method to build and run the service by `Makefile` in a sources root.
run `make` with one of below listed `Target` to execute an`Action`:

**Make targets**

| Target            |         Action                             |
|-------------------|--------------------------------------------|
| go_test_unit      | run unit tests. | 
| build             | run unit tests and build the service binary file.              |
| docker_image      | build a docker image using *Go 1.12* image and create a new image with service binary. | 
| cluster_start     | build and start a cluster of all required docker containers (`PostgreSQL` and `Coins Wallet service`).|
| cluster_stop      | stop the cluster and remove according docker containers. |  
| cluster_restart   | cluster_stop, then cluster_start. | 
| cluster_run_tests | run functional tests on a running cluster. | 
| cluster_test      | cluster build, run, run functional tests. | 

**Most usual scenarios:**
- Build binary: Run `make build` to get a `coins-wallet` binary in a project root.
- Run & test: Run `make cluster_start` to get a running containers with service listening a local port `8080`.

**NOTE** you could manage cluster parameters by change according valules inside `build/coins-wallet.env` file.

# Endpoints
## GET /accounts
Used to get an information about all accounts registered in a system.
Also allows to be used to filter a list by categories. 
**NOTE** currently supported only `currency` category.

**Request info**
```
HTTP Request method    GET
Request URL            http://127.0.0.1:8088/accounts
```
Returns all accounts.

```
HTTP Request method    GET
Request URL            http://127.0.0.1:8088/accounts/currency/USD
```
Returns only USD currency accounts.

**Response**
```
{
    "accounts":[{*account info*},{*account info*},...],
    [,"error": "*error-message*"]
}
```
Where `account info` is a JSON of view:
```
    {
        "name": "Picachu",
        "currency": "JPY",
        "balance": 73800
    }
```

## GET /payments
Used to get an information about all paymentss registered in a system.

**Request info**
```
HTTP Request method    GET
Request URL            http://127.0.0.1:8088/payments
```
Returns all payments.

**Response**
```json
{
    "payments": [
        {
            "uid": "68f32b4d-9d3e-495c-8957-dc2861b4db4e",
            "from": "Alfred01-Us",
            "to": "Sam1999",
            "amount": 3.05,
            "currency": "USD",
            "reason": "charity 01",
            "created_at": "2019-08-08T18:29:14.734875Z",
            "status": "done"
        },
        {
            "uid": "4094c5cb-bed4-462b-87af-5ff97d676760",
            "from": "Alfred01-Us",
            "to": "Sam1999",
            "amount": 11333.05,
            "currency": "USD",
            "reason": "charity 01",
            "created_at": "2019-08-08T18:29:30.911409Z",
            "status": "failed"
        }
        ],
    "error": "error-message"
}
```
Where `error` is optional


## POST /payment
Used to make initiate a payment from one account to another of the same currency.
In case of source and target accounts have a different currencies, the payment will be rejected.
In case payment amount is bigger than a source account balance - the payment will be saved to a system and marked as `Failed`. According error will be returned with an information about payment failed. 

**Request info**
```
HTTP Request method    POST
Request URL            http://127.0.0.1:8088/payment
```

**Request body**
```json
{
    "from": "Alfred01-Us",
    "to": "Sam1999",
    "amount": 3.05,
    "reason": "thank you",
}
```

**Response**
```json
{
    "payments": [{
            "uid": "68f32b4d-9d3e-495c-8957-dc2861b4db4e",
            "from": "Alfred01-Us",
            "to": "Sam1999",
            "amount": 3.05,
            "currency": "USD",
            "reason": "charity 01",
            "created_at": "2019-08-08T18:29:14.734875Z",
            "status": "done"
        }],
    "error": "error-message"
}
```
Where `error` is optional
